\documentclass[12pt,french,a4paper,titlepage]{exam}

\usepackage[pdftex]{hyperref}
\usepackage{listings}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{url}
\usepackage[french]{babel}
\usepackage{varioref}
\usepackage[vmargin=3cm]{geometry}
\usepackage[pdftex]{graphicx}
\usepackage{listings}
\lstset{%
  numbers=left,%
  basicstyle=\scriptsize\ttfamily,%
  numbersep=25pt,%
  frame=trBL,%
}

\title{Multimedia\\ {\footnotesize ISN, Lycée André \textsc{Malraux} -- 72700 Allonnes}}
\author{Grégory DAVID}

\setlength\answerlinelength{7cm}

\begin{document}
\maketitle{}
%\printanswers{}
\part{Découverte de \emph{The GIMP} et d'une image discrète}
\label{sec:theGimp}

\begin{questions}
    \question%
    Exécuter l'application \texttt{The GIMP} présente sur le
    bureau. Vous devriez avoir une interface semblable à la
    \figurename\ \vref{fig:gimpInterface}.
    \begin{figure}[p]
        \centering
        \includegraphics[width=12cm]{data/gimp_interface}
        \caption{Interface Homme Machine (IHM) de l'application
          \texttt{The GIMP}}
        \label{fig:gimpInterface}
    \end{figure}

    \question%
    Ouvrir le fichier disponible à l'adresse
    \url{https://framagit.org/isn-malraux/TP2-image-discrete/raw/master/data/gimp_image_reference.png}
    semblable à la \figurename\ \vref{fig:imageDeReference}
    \begin{figure}[p]
        \centering
        \includegraphics[height=10cm]{data/gimp_image_reference}
        \label{fig:imageDeReference}
        \caption{Image de référence à ouvrir avec \texttt{The GIMP}}
    \end{figure}

    \question%
    Identifier les propriétés suivantes de l'image :
    \begin{parts}
        \part%
        dimensions en \emph{pixels}
        \answerline[$1024 \times 1534 \text{pixels}$]

        \part%
        résolution \answerline[$300 \times 300 \text{ppp}$]

        \part%
        taille d'impression
        \answerline[$86.70 \times 129.88 \text{mm}$]

        \part%
        espace de couleurs \answerline[\texttt{RVB}]
    \end{parts}

    \question%
    En déduire les définitions suivantes :
    \begin{parts}
        \part%
        \emph{pixel} :
        \begin{solutionorlines}[3cm]
            Un \emph{PICture ELement}, nommé \emph{pixel}, est un
            point constitutif d'une image. Il est composé d'une ou
            plusieurs valeurs définissant alors le niveau de
            luminosité de la (des) composante(s) colorimétriques. Par
            exemple, une image couleur en \texttt{RVB} est constituée
            d'un ensemble de \emph{pixels} contenant chacun trois
            valeurs : une pour le \texttt{R}(ouge), une pour le
            \texttt{V}(ert) et une pour le \texttt{B}(leu).
        \end{solutionorlines}

        \part%
        dimensions en \emph{pixel} :
        \begin{solutionorlines}[3cm]
            Le nombre de \emph{pixels} en largeur et en hauteur
            permettant de décrire la superficie du rectangle de
            l'image. Elle s'exprime de la façoon suivante :
            $$ l_{pixel} \times h_{pixel} $$
        \end{solutionorlines}

        \part%
        résolution :
        \begin{solutionorlines}[3cm]
            La résolution est une information permettant de connaître
            la densité de points (ou de \emph{pixel} utilisés par
            unité physique, lors du passage de l'immatériel au
            matériel (de l'image numérique au papier, à l'écran,
            etc.). Elle est exprimée en \texttt{ppp} (point par pouce)
            ou \texttt{dpi} (\emph{dot per inch}) et peut être
            différente selon les axes ($x$ ou $y$). On la représente
            de la façon suivante :
            $$ r_{x} \times r_{y} $$
        \end{solutionorlines}

        \part%
        taille d'impression (envisager une formule de calcul à partir
        de la taille et de la résolution) :
        \begin{solutionorlines}[3cm]
            Tout comme les dimensions de l'image numérique, la taille
            d'impression correspond aux dimensions de l'image
            physique. Elle est exprimée en \texttt{pouce} ou
            \texttt{inch} ($1 \text{pouce} = 2.54\text{cm}$). Nous
            pouvons en déduire la formule suivante :
            $$ l_{impression}\text{inch} = \frac{l_{pixel}}{r_{x}} = 2.54 \times \frac{l_{pixel}}{r_{x}} \text{cm} $$
            $$ h_{impression}\text{inch} = \frac{h_{pixel}}{r_{y}} = 2.54 \times \frac{h_{pixel}}{r_{y}} \text{cm} $$
        \end{solutionorlines}

        \part%
        espace de couleurs :
        \begin{solutionorlines}[3cm]
            Défini la nature colorimétrique de l'image et par
            conséquent la façon dont vont être enregistrées les
            données numériques caractérisant chaque pixel.En peut
            citer \texttt{RVB}, \texttt{CMJN}, \texttt{N\&B},
            \texttt{Niveau de gris} ou encore \texttt{Palette de
              couleurs}.
        \end{solutionorlines}
    \end{parts}

    \question%
    Zommer sur l'image jusqu'à $800 \%$. Que remarquez-vous ?
    \begin{solutionorlines}[3cm]
        Nous pouvons remarquer que l'image est constituée d'un
        ensemble de carrés : les \emph{pixels}. L'image ne semble plus
        être cohérente à ce niveau de zoom. Le \emph{pixel} apparaît
        alors comme l'élément atomique de l'image.
    \end{solutionorlines}

    \question%
    En déduire une définition de la notion d'\textbf{image discrète} ?
    \begin{solutionorlines}[5cm]
        Alors nous pouvons considérer que si l'image cohérente est un
        ensemble d'éléments singuliers (les \emph{pixels}), c'est
        qu'il y a une discrétisation de l'information. Il n'y a donc
        pas de continuité dans l'information, le signal, l'image n'est
        qu'un assemblage de points, comme la technique de pointillisme
        en peinture (voir
        \url{https://fr.wikipedia.org/wiki/Pointillisme}.
    \end{solutionorlines}

    \clearpage

    \uplevel{\part{Travail pratique}
      \label{sec:pratique}
      \section{Manipulation d'image}
      \label{sec:manipulationImage}
    }%
    \question%
    \label{ques:creerImagePBM}%
    Créer une nouvelle image avec les contraintes suivants:
    \begin{parts}
        \part%
        \begin{description}
            \item[Taille d'image :] $10 \times 10 \textrm{pixels}$
            \item[Résolution :] $5 \times 5 \textrm{ppp}$
            \item[Espace de couleurs :] \texttt{Couleur RVB}
            \item[Remplir avec :] \texttt{Blanc}
        \end{description}


        \part%
        contenant sur la première ligne un damier alterné entre
        \texttt{Noir} et \texttt{Blanc}

        \part%
        exporter l'image (\texttt{Fichier} $\rightarrow$
        \texttt{Exporter Sous ...}) au format \texttt{.pbm} en mode
        \texttt{ASCII}
    \end{parts}

    \question%
    \label{ques:analyserFichierPBM}%
    À l'aide d'un éditeur de texte (capable de lire les fichiers avec
    les fins de ligne UNIX : \texttt{spyder3}, \texttt{gedit} ou
    \texttt{geany} par exemple), ouvrir le fichier \texttt{.pbm}
    nouvellement créé (comme dans le
    \lstlistingname~\vref{lst:fichierPBM}). Que remarquez-vous ?
    \begin{solutionorlines}[3cm]
        Il y a une succession de valeurs numériques : 0 ou 1. Nous
        pouvons remarquer aussi que le noir est codé en la valeur 1 et
        le blanc en la valeur 0.

        Le début du fichier contient des informations structurelles,
        telle que la dimension de l'image en ligne 3. Par ailleurs, la
        première ligne semble contenir une information de type sur le
        fichier.

        Enfin, nous pouvons remarquer que les \emph{pixels} sont
        écrits les uns à la suite des autres, sans toutefois faire
        dépasser une ligne de plus de 70 caractères.
    \end{solutionorlines}
    \lstinputlisting[caption={Contenu textuel du fichier
      \texttt{.pbm}},label={lst:fichierPBM}]{data/gimp_damier_manuel.pbm}

    \question%
    \label{ques:modifierFichierPBMfin}%
    Modifier les dix derniers zéros ($0$) et les remplacer par des uns
    ($1$), sauvegarder le fichier et l'ouvrir de nouveau avec
    \texttt{The GIMP}. Que remarquez-vous ?
    \begin{solutionorlines}[3cm]
        La dernière ligne de l'image est devenue noire.
    \end{solutionorlines}

    \question%
    \label{ques:modifierFichierPBMtotal}%
    Modifier, dans l'éditeur de texte, le fichier \texttt{.pbm} de
    manière à concevoir manuellement une image de damier complet sur
    l'ensemble de l'image.

    \question%
    \label{ques:redoPNM}%
    Refaire le travail de
    \vrefrange{ques:creerImagePBM}{ques:analyserFichierPBM} en
    exportant en fichier \texttt{.pnm} mode \texttt{ASCII} à la
    place. Que remarquez-vous ?
    \begin{solutionorlines}[4cm]
        Nous pouvons remarquer que les informations ne sont plus dans
        le même format. En effet, le type (ligne 1) est devenu
        \texttt{P3}, tandis qu'il était \texttt{P1} auparavant.

        Par ailleurs, la ligne 4 contient une information structurelle
        que le type \texttt{P1} ne contenait pas : la valeur maximale
        d'une composante colorimétrique. En effet, la valeur
        \texttt{255} à la ligne 4 détermine la valeur maximale pour le
        \texttt{R}(ouge), le \texttt{V}(ert) et le \texttt{B}(leu) de
        chacun des \emph{pixels}.

        Les \emph{pixels} sont écrits par paquet de 3 valeurs, l'une
        pour le \texttt{R}, une autre pour le \texttt{V}, la dernière
        pour le \texttt{B}. Par conséquent pour cette image de
        $10 \times 10$, nous avons $3 \times 10 \times 10$ valeurs
        numériques stockées dans le fichier pour représenter, en
        couleur \texttt{RVB}, l'ensemble des 100 \emph{pixels} de
        l'image.
    \end{solutionorlines}

    \question%
    \label{ques:modifierFichierPNMdegrade}%
    Modifier alors le fichier \texttt{.pnm} dans l'éditeur de texte
    afin de réaliser une image d'un dégradé du \texttt{Noir} vers le
    \texttt{Blanc}, de gauche à droite respectivement, tel que visible
    dans la \figurename\ \vref{fig:imageDegrade}.
    \begin{figure}[p]
        \centering
        \fbox{\includegraphics[width=10cm]{data/gimp_image_degrade.png}}
        \caption{Image d'un dégradé du \texttt{Noir} vers le
          \texttt{Blanc} à produire à la question
          \vref{ques:programmePythonPNMdegrade}}
        \label{fig:imageDegrade}
    \end{figure}

    \uplevel{%
      \section{Génération programmatique d'image}
      \label{sec:generationProgrammatiqueImage}
    }%
    \question%
    \label{ques:programmePythonPBMdamier}%
    À partir des manipulations faites dans les questions
    \vrefrange{ques:creerImagePBM}{ques:modifierFichierPBMtotal} de la
    section \vref{sec:manipulationImage}, envisager un programme écrit
    en \texttt{Python3} permettant de générer le contenu textuel d'une
    image au format \texttt{.pbm} illustrant un damier de
    $10 \times 10 \textrm{pixels}$ avec la première case en
    \texttt{Blanc}.
    \begin{solution}
        \lstinputlisting[language=Python,caption={Programme en
          \texttt{Python3} permettant de générer une image de damier
          au format \texttt{.pbm}}]{data/damierPBM.py}
    \end{solution}

    \question%
    \label{ques:programmePythonPNMdegrade}%
    À partir des manipulations faites dans les questions
    \vrefrange{ques:redoPNM}{ques:modifierFichierPNMdegrade} de la
    section \vref{sec:manipulationImage}, envisager un programme écrit
    en \texttt{Python3} permettant de générer le contenu textuel d'une
    image au format \texttt{.pnm} de $10 \times 10 \textrm{pixels}$,
    illustrant un dégradé du \texttt{Blanc} vers le \texttt{Noir}, de
    gauche à droite respectivement.
    \begin{solution}
        \lstinputlisting[language=Python,caption={Programme en
          \texttt{Python3} permettant de générer une image de dégradé
          du \texttt{Blanc} vers le \texttt{Noir} au format
          \texttt{.pnm}}]{data/degradePNM.py}
    \end{solution}

    \question%
    \label{ques:programmePythonPNMdegradeCouleur}%
    À partir du programme écrit à la question
    \vref{ques:programmePythonPNMdegrade}, améliorer le programme afin
    de générer le contenu textuel d'une image au format \texttt{.pnm}
    de $10 \times 10 \textrm{pixels}$, illustrant un dégradé du
    \texttt{Jaune} vers le \texttt{Noir}, de gauche à droite
    respectivement.
    \begin{solution}
        \lstinputlisting[language=Python,caption={Programme en
          \texttt{Python3} permettant de générer une image de dégradé
          du \texttt{Jaune} vers le \texttt{Noir} au format
          \texttt{.pnm}}]{data/degradePNMcolor.py}
    \end{solution}

\end{questions}
\nomorequestions{}
\end{document}
