MAX = int(255)
MIN = int(0)
tailleX = int(10)
tailleY = int(10)
longueurLigne = int(70)
pasDuDegrade = MAX / (tailleX - 1)

# Jaune = 100%(Rouge) + 100%(Vert) + 0%(Bleu) [en synthese additive]
R = MAX
V = MAX
B = MIN

"""Le format du fichier (son Magic Number)"""
print("P3")
"""Les dimensions de l'image"""
print(tailleX, tailleY)
"""La profondeur d'encodage d'une composante Rouge, Verte ou Bleue"""
print(MAX)

for compteurY in range(0, tailleY):
    for compteurX in range(0, tailleX):
        if(compteurX == 0):
            R = MAX
            V = MAX
        print(int(R),end=' ')
        print(int(V),end=' ')
        print(int(B))
        R -= pasDuDegrade
        V -= pasDuDegrade
