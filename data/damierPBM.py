BLANC = int(0)
tailleX = int(10)
tailleY = int(10)
longueurLigne = (70)
pixelCourant = BLANC

"""Le format du fichier (son Magic Number)"""
print("P1")
"""Les dimensions de l'image"""
print(tailleX, tailleY)

for compteurY in range(0, tailleY):
    for compteurX in range(0, tailleX):
        print(pixelCourant, end='')
        if((compteurX + compteurX * compteurY) % tailleX != 0):
            pixelCourant = (pixelCourant + 1) % 2
        if((compteurX + 1) % longueurLigne == 0):
            print(end='\n')
    print(end='\n')
