BLANC = int(255)
tailleX = int(10)
tailleY = int(10)
longueurLigne = int(70)
pasDuDegrade = BLANC / (tailleX - 1)
pixelCourant = BLANC

"""Le format du fichier (son Magic Number)"""
print("P3")
"""Les dimensions de l'image"""
print(tailleX, tailleY)
"""La profondeur d'encodage d'une composante Rouge, Verte ou Bleue"""
print(BLANC)

for compteurY in range(0, tailleY):
    for compteurX in range(0, tailleX):
        if(compteurX == 0):
            pixelCourant = BLANC
        for i in range(0, 3):
            print(int(pixelCourant))
        pixelCourant = pixelCourant - pasDuDegrade
